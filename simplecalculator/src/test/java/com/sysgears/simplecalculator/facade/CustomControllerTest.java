package com.sysgears.simplecalculator.facade;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by sirenko on 28.07.15.
 */
public class CustomControllerTest {

    @org.junit.Test
    public void testProcessExpression() throws Exception {
        List<ControllerTestCase> testCaseList = new ArrayList<>();
        testCaseList.add(new ControllerTestCase("42", 42));
        testCaseList.add(new ControllerTestCase("2+2", 4));
        testCaseList.add(new ControllerTestCase("42^0", 1));
        testCaseList.add(new ControllerTestCase("-42", -42));
        testCaseList.add(new ControllerTestCase("-2-2", -4));
        testCaseList.add(new ControllerTestCase("2+2*2", 6));
        testCaseList.add(new ControllerTestCase("-2-(-2)", 0));
        testCaseList.add(new ControllerTestCase("2^-2", 0.25));
        testCaseList.add(new ControllerTestCase("2*(2+3)", 10));
        testCaseList.add(new ControllerTestCase("2^0.325", 1.252664));
        testCaseList.add(new ControllerTestCase("2^-0.25", 0.840896));
        testCaseList.add(new ControllerTestCase("2*(2+3)+3*(1+4^2)", 61));
        testCaseList.add(new ControllerTestCase("7*24+14*(2+(1-8)*3)/2^2", 101.5));
        testCaseList.add(new ControllerTestCase(" 2 +  2  ", 4));

        CalculatorFacade calculatorFacade = new CalculatorFacade();
        for (ControllerTestCase testCase : testCaseList) {
            System.out.println(String.format("Testing '%s'", testCase.getExpression()));
            double startTime = System.currentTimeMillis();
            List<String> resultList = calculatorFacade.processExpression(testCase.getExpression());
            assertTrue(resultList.size() > 0);
            String strResult = resultList.get(0);
            double endTime = System.currentTimeMillis();
            System.out.println(String.format("%s expected, %s received at %s ms", testCase.getResult(), strResult, endTime - startTime));
            double result;
            try {
                result = Double.valueOf(strResult);
            } catch (NumberFormatException e) {
                throw new AssertionError(e);
            }
            assertTrue(testCase.checkResult(result));
        }
    }

    private static class ControllerTestCase {
        private final String expression;
        private final double result;

        public ControllerTestCase(String expression, double result) {
            this.expression = expression;
            this.result = result;
        }

        public String getExpression() {
            return expression;
        }

        public double getResult() {
            return result;
        }

        public boolean checkResult(double result) {
            return (Math.abs(result - this.result) < 0.000001);
        }
    }
}