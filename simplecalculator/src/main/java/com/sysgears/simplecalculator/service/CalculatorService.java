package com.sysgears.simplecalculator.service;

import com.sysgears.simplecalculator.common.CalculatorConstants;
import com.sysgears.simplecalculator.exceptions.EvaluateException;
import com.sysgears.simplecalculator.exceptions.ParseException;
import com.sysgears.simplecalculator.exceptions.PostfixConvertException;
import com.sysgears.simplecalculator.history.CalculationHistoryDecorator;
import com.sysgears.simplecalculator.history.IHistory;
import com.sysgears.simplecalculator.tokenprocessors.evaluator.TokenListEvaluator;
import com.sysgears.simplecalculator.tokenprocessors.parser.ExpressionParser;
import com.sysgears.simplecalculator.tokenprocessors.sorter.TokenListPostfixConverter;
import com.sysgears.simplecalculator.tokens.Token;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic calculation service class.
 * <p>
 * Provides step-by-step calculation of the math expression.
 */
public class CalculatorService {

    /** Expression parser */
    private final ExpressionParser parser;

    /** Converter of parsed expression to postfix form */
    private final TokenListPostfixConverter converter;

    /** Final evaluator of postfix expression */
    private final TokenListEvaluator evaluator;

    /** Calculation history holder */
    private final IHistory history;

    /**
     * Creates an instance of service class
     *
     * @param parser expression parser
     * @param converter expression infix-to-postfix converter
     * @param evaluator postfix expression evaluator
     * @param history calculation history holder
     */
    public CalculatorService(ExpressionParser parser, TokenListPostfixConverter converter,
                             TokenListEvaluator evaluator, IHistory history) {
        this.parser = parser;
        this.converter = converter;
        this.evaluator = evaluator;
        this.history = history;
    }

    /**
     * Calculate string expression.
     * <p>
     * In case of history parameter received will return calculation history
     * otherwise will try to calculate provided expression.
     * In case of error will return an error string.
     *
     * May accept commands such as
     * {@value CalculatorConstants#COMMAND_GET_HISTORY} to read whole history,
     * {@value CalculatorConstants#COMMAND_GET_HISTORY_UNIQUE} to read unique history,
     * {@value CalculatorConstants#COMMAND_GET_HISTORY_UNIQUE} to read last history item
     *
     * @param expression math expression or program command, not null
     * @return list of result strings, not null
     */
    public List<String> processExpression(final String expression) {
        if (CalculatorConstants.COMMAND_GET_HISTORY.equals(expression)) {
            return history.getAll();
        } else if (CalculatorConstants.COMMAND_GET_HISTORY_UNIQUE.equals(expression)) {
            return history.getUnique();
        } else if (CalculatorConstants.COMMAND_GET_HISTORY_LAST.equals(expression)) {
            return history.getLast(1);
        } else {
            return calculate(expression);
        }
    }

    /**
     * Calculate math expression step-by-step and save it to the history
     *
     * @param expression math expression, not null
     * @return list of result strings, not null
     */
    private List<String> calculate(final String expression) {
        List<String> resultList = new ArrayList<>();
        try {
            List<Token> list;
            list = parser.parseExpressionToTokensList(expression);
            list = converter.convertInfixToPostfix(list);
            double result = evaluator.evaluate(list);
            history.add(String.format("%s = %s", expression, result));
            resultList.add(String.valueOf(result));
        } catch (ParseException | PostfixConvertException | EvaluateException e) {
            resultList.add(e.getMessage());
        }
        return resultList;
    }
}
