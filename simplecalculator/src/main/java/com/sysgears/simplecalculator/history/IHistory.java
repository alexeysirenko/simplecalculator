package com.sysgears.simplecalculator.history;

import java.util.List;

/**
 * History holder interface
 */
public interface IHistory {

    /**
     * Add a line to the end of history list
     *
     * @param item a string to remember, not null
     */
    void add(final String item);

    /**
     * Return specific number of last items or less if history does contains that many.
     *
     * @return history list, empty in case of no history written, not null
     */
    List<String> getLast(final int count);

    /**
     * Return entire history list
     *
     * @return history list, not null
     */
    List<String> getAll();

    /**
     * Return history list of unique expressions
     *
     * @return history list, not null
     */
    List<String> getUnique();
}
