package com.sysgears.simplecalculator.tokenprocessors.evaluator;

import com.sysgears.simplecalculator.tokens.operators.math.MathOperatorToken;
import com.sysgears.simplecalculator.tokens.numbers.NumericToken;
import com.sysgears.simplecalculator.tokens.Token;
import com.sysgears.simplecalculator.exceptions.EvaluateException;

import java.util.*;

/**
 * Evaluator of math expression presented in postfix form
 */
public class TokenListEvaluator {

    /**
     * Evaluate provided math expression presented in postfix form
     *
     * @param input expression in postfix form as list of tokens, not null
     * @return calculated number
     * @throws EvaluateException in case of invalid input expression
     */
    public double evaluate(List<Token> input) throws EvaluateException {
        Deque<Double> operandStack = new ArrayDeque<>();
        // Enumerate tokens, push numbers to stack, pop back in case of math operator found,
        // push calculated result back to stack
        for (Token token : input) {
            if (isNumber(token)) {
                NumericToken number = (NumericToken) token;
                operandStack.push(number.getValue());
            } else if (isMathOperator(token)) {
                MathOperatorToken operator = (MathOperatorToken) token;
                if (operandStack.size() < operator.getOperandsCount()) {
                    throw new EvaluateException("Lack of operands");
                }
                double[] operands = new double[operator.getOperandsCount()];
                for (int i = operands.length - 1; i >= 0; i--) {
                    operands[i] = operandStack.pop();
                }
                try {
                    double result = operator.calculate(operands);
                    operandStack.push(result);
                } catch (ArithmeticException e) {
                    throw new EvaluateException(e);
                }
            } else {
                throw new IllegalArgumentException(String.format("Unexpected token '%s'", token));
            }
        }
        if (operandStack.size() > 1) {
            throw new EvaluateException("Lack of operators");
        } else if (operandStack.size() == 0) {
            throw new EvaluateException("Empty result set");
        }
        return operandStack.pop();
    }

    /**
     * Check if provided token is number
     *
     * @param token expression token
     * @return result
     */
    private boolean isNumber(Token token) {
        return token instanceof NumericToken;
    }

    /**
     * Check if provided token is math operator
     *
     * @param token expression token
     * @return result
     */
    private boolean isMathOperator(Token token) {
        return token instanceof MathOperatorToken;
    }
}
