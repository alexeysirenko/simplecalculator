package com.sysgears.simplecalculator.tokenprocessors.parser;

import com.sysgears.simplecalculator.tokens.numbers.NumericToken;
import com.sysgears.simplecalculator.tokens.operators.math.executor.MathOperator;
import com.sysgears.simplecalculator.tokens.operators.parentheses.ParenthesesOperatorToken;
import com.sysgears.simplecalculator.tokens.Token;
import com.sysgears.simplecalculator.exceptions.ParseException;
import com.sysgears.simplecalculator.tokens.operators.math.MathOperatorToken;

import java.util.ArrayList;
import java.util.List;


/**
 * Parses provided math expression to list of operators and operands (tokens),
 * perform initial expression validation
 */
public class ExpressionParser {

    /**
     * Parse provided string expression
     *
     * @param expression mathematical expression, not null
     * @return list of parsed tokens, not null
     * @throws ParseException in case of invalid characters in input expression
     */
    public List<Token> parseExpressionToTokensList(String expression) throws ParseException {
        List<Token> tokenList = new ArrayList<>();
        StringBuilder input = new StringBuilder();
        StringBuilder numBuffer = new StringBuilder();
        input.append(expression.replaceAll("\\s+", "")); // Remove whitespaces
        if (input.length() == 0) {
            throw new ParseException("Empty input string");
        }
        boolean isUnary = true; // Required to recognise unary minus
        do {
            String s = String.valueOf(input.charAt(0));
            input.delete(0, 1);

            if (isNumber(s)) {
                numBuffer.append(s);
            }
            // If non-numeric character found save previously found number to result list
            if (numBuffer.length() > 0 && (!isNumber(s) || input.length() == 0)) {
                try {
                    tokenList.add(NumericToken.valueOf(numBuffer.toString()));
                } catch (NumberFormatException e) {
                    throw new ParseException(String.format("'%s' is not a valid double value", numBuffer.toString()));
                }
                numBuffer.setLength(0);
            }
            // Replace unary minus with individual unary operator
            if (isUnary && s.equals(MathOperator.MINUS.toString())) {
                s = MathOperator.UNARYMINUS.toString();
            }
            if (isMathOperator(s)) {
                tokenList.add(MathOperatorToken.valueOf(s));
            } else if (isParentheses(s)) {
                tokenList.add(ParenthesesOperatorToken.valueOf(s));
            } else if (!isNumber(s)) {
                throw new ParseException(String.format("Unknown character '%s'", s));
            }
            isUnary = !isNumber(s);
        } while (input.length() > 0);
        return tokenList;
    }

    /**
     * Check is provided string a number
     *
     * @param s checked string, not null
     * @return result
     */
    private boolean isNumber(String s) {
        return NumericToken.isNumber(s);
    }

    /**
     * Check is provided string a math operator
     *
     * @param s checked string, not null
     * @return result
     */
    private boolean isMathOperator(String s) {
        return MathOperatorToken.isOperatorSupported(s);
    }

    /**
     * Check is provided string a parentheses
     *
     * @param s checked string, not null
     * @return result
     */
    private boolean isParentheses(String s) {
        return ParenthesesOperatorToken.isOperatorSupported(s);
    }
}
