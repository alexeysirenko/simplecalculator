package com.sysgears.simplecalculator;

import com.sysgears.simplecalculator.common.CalculatorConstants;
import com.sysgears.simplecalculator.facade.CalculatorFacade;

import java.util.List;
import java.util.Scanner;

/**
 * Main application class
 */
public class Application {

    /** Entry point */
    public static void main(String[] args) {
        try {
            Scanner scanner = new Scanner(System.in);
            CalculatorFacade calculatorFacade = new CalculatorFacade();
            while (true) {
                System.out.println(">");
                System.out.println("Press Enter to exit");
                System.out.println(String.format("Type '%s', '%s' or '%s' to receive all, unique or the last result of calculation history",
                        CalculatorConstants.COMMAND_GET_HISTORY,
                        CalculatorConstants.COMMAND_GET_HISTORY_UNIQUE,
                        CalculatorConstants.COMMAND_GET_HISTORY_LAST));
                System.out.println("Input expression:");
                String expression = scanner.nextLine();
                if (expression.isEmpty()) {
                    return;
                }
                List<String> result = calculatorFacade.processExpression(expression);
                for (String line : result) {
                    System.out.println(line);
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
