package com.sysgears.simplecalculator.tokens.operators.math.executor;

import com.sysgears.simplecalculator.common.CalculatorConstants;

/**
 * Enum class to store all math operators
 */
public enum MathOperator {

    /** Summing */
    PLUS("+", MathOperator.BINARY, CalculatorConstants.OPERATOR_PRIORITY_DEFAULT + 1) {

        /**
         * Calculate operands addition
         *
         * @param args operands
         * @return addition result
         */
        public double calculate(double[] args) {
            return args[0] + args[1];
        }
    },

    /** Subtraction */
    MINUS("-", MathOperator.BINARY, PLUS.getPriority()) {

        /**
         * Calculate operands subtraction
         *
         * @param args operands
         * @return subtraction result
         */
        public double calculate(double[] args) {
            return args[0] - args[1];
        }
    },

    /** Multiplication */
    MULTIPLY("*", MathOperator.BINARY, PLUS.getPriority() + 1) {

        /**
         * Calculate operands multiplication
         *
         * @param args operands
         * @return multiplication result
         */
        public double calculate(double[] args) {
            return args[0] * args[1];
        }
    },

    /** Division */
    DIVIDE("/", MathOperator.BINARY, MULTIPLY.getPriority()) {

        /**
         * Calculate operands division
         *
         * @param args operands
         * @return division result
         */
        public double calculate(double[] args) {
            return args[0] / args[1];
        }
    },

    /** Exponentiation */
    POWER("^", MathOperator.BINARY, MULTIPLY.getPriority() + 1) {

        /**
         * Calculate operands exponentiation
         *
         * @param args operands
         * @return exponentiation result
         */
        public double calculate(double[] args) {
            return Math.pow(args[0], args[1]);
        }
    },

    /** Unary minus (sign change) */
    UNARYMINUS("±", MathOperator.UNARY, POWER.getPriority() + 1) {

        /**
         * Calculate sign change
         *
         * @param args operands
         * @return sign change result
         */
        public double calculate(double[] args) {
            return -args[0];
        }
    }
    ;

    /** Required operands count constants */
    private static final int UNARY = 1;
    private static final int BINARY = 2;

    /** Operator name */
    private final String operatorName;

    /** Number of required operands */
    private final int operandsCount;

    /** Operator priority */
    private final int priority;

    /**
     * Creates enum instance
     *
     * @param operatorName operator name
     * @param operandsCount number of required operands
     * @param priority operator priority
     */
    MathOperator(String operatorName, int operandsCount, int priority) {
        this.operatorName = operatorName;
        this.operandsCount = operandsCount;
        this.priority = priority;
    }

    /**
     * Calculate expression method
     *
     * @param args operator arguments
     * @return calculation result
     */
    public abstract double calculate(double[] args);

    /**
     * Return number of operands required for current operator
     *
     * @return number of required operands
     */
    public int getOperandsCount() {
        return operandsCount;
    }

    /**
     * Return operator priority
     *
     * @return operator priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Return enum instance by math operator name
     *
     * @param name math operator name, not null
     * @return enum instance, not null
     */
    public static MathOperator byOperatorName(String name) throws IllegalArgumentException {
        for (MathOperator executor: MathOperator.values()) {
            if (executor.toString().equals(name)) {
                return executor;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid math operator name '%s'", name));
    }

    /**
     * This returns a String representation of the MathOperator
     *
     * @return string representation
     */
    public String toString() {
        return operatorName;
    }
}