package com.sysgears.simplecalculator.tokens.numbers;

import com.sysgears.simplecalculator.tokens.Token;

/**
 * Token to store numeric values, immutable
 */
public class NumericToken extends Token {

    /** Token value */
    private final double value;

    /**
     * Creates an instance of class
     *
     * @param value number value
     */
    private NumericToken(double value) {
        this.value = value;
    }

    /**
     * Creates an instance of class using provided number string
     *
     * @param value number value
     * @return instance of created token, not null
     * @throws NumberFormatException in case of string does not contain numeric value
     */
    public static NumericToken valueOf(String value) throws NumberFormatException {
        return new NumericToken(Double.parseDouble(value));
    }

    /**
     * Creates an instance of class using provided number
     *
     * @param value umber value
     * @return instance of created token, not null
     */
    public static NumericToken valueOf(double value) {
        return new NumericToken(value);
    }

    /**
     * Token value getter
     *
     * @return token value
     */
    public double getValue() {
        return value;
    }

    /**
     * Check is provided string a part of a number
     *
     * @param operator checked string, not null
     * @return result
     */

    public static boolean isNumber(String operator) {
        return operator.matches("^[0-9\\.]+?$");
    }

    /**
     * This will return a deep clone of this NumericToken
     *
     * @return new cloned instance
     */
    @Override
    public NumericToken clone() {
        return NumericToken.valueOf(this.getValue());
    }

    /**
     * This returns a String representation of the NumericToken
     *
     * @return string presentation
     */
    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
