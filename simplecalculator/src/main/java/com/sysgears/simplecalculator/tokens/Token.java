package com.sysgears.simplecalculator.tokens;

/**
 * Basic class to store math expression elements (for inheritance)
 */
public abstract class Token implements Cloneable {

    /**
     * This will return a clone of instance
     *
     * @return new cloned instance
     */
    public Token clone() {
        try {
            return (Token) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }
}
