package com.sysgears.simplecalculator.tokens.operators.parentheses.executor;

import com.sysgears.simplecalculator.common.CalculatorConstants;

/**
 * Enum class to store all available expression parentheses
 */
public enum ParenthesesOperator {

    /** Open round bracket */
    OPEN("(", CalculatorConstants.OPERATOR_PRIORITY_DEFAULT, ParenthesesType.OPEN),

    /** Close round bracket */
    CLOSE(")", CalculatorConstants.OPERATOR_PRIORITY_DEFAULT, ParenthesesType.CLOSE)
    ;

    /** Parentheses type */
    private enum ParenthesesType {OPEN, CLOSE}

    /** Operator name */
    private final String operatorName;

    /** Operator priority */
    private final int priority;

    /** Current parentheses type */
    private final ParenthesesType type;

    /**
     * Creates enum instance
     *
     * @param operatorName operator name
     * @param priority operator priority
     * @param type parentheses type
     */
    ParenthesesOperator(String operatorName, int priority, ParenthesesType type) {
        this.operatorName = operatorName;
        this.priority = priority;
        this.type = type;
    }

    /**
     * Return enum instance by parentheses operator name
     *
     * @param name parentheses operator name, not null
     * @return enum instance, not null
     */
    public static ParenthesesOperator byOperatorName(String name) throws IllegalArgumentException {
        for (ParenthesesOperator executor: ParenthesesOperator.values()) {
            if (executor.toString().equals(name)) {
                return executor;
            }
        }
        throw new IllegalArgumentException(String.format("Invalid parenthesis operator name '%s'", name));
    }

    /**
     * Get operator priority
     *
     * @return operator priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * This returns a String representation of the instance
     *
     * @return string presentation
     */
    public String toString() {
        return operatorName;
    }

    /**
     * This checks for opening parentheses
     *
     * @return check result
     */
    public boolean isOpen() {
        return type == ParenthesesType.OPEN;
    }
}
