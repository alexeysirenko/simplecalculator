package com.sysgears.simplecalculator.exceptions;

/**
 * Exception during converting infix expression to postfix form
 */
public class PostfixConvertException extends Exception {

    /**
     * Creates instance of the exception
     *
     * @param e parent exception
     */
    public PostfixConvertException(Exception e) {
        super(e);
    }

    /**
     * Creates instance of the exception
     *
     * @param message error message
     */
    public PostfixConvertException(String message) {
        super(message);
    }
}
