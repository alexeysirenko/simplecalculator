package com.sysgears.simplecalculator.facade;


import com.sysgears.simplecalculator.history.CalculationHistoryContainer;
import com.sysgears.simplecalculator.history.CalculationHistoryDecorator;
import com.sysgears.simplecalculator.history.IHistory;
import com.sysgears.simplecalculator.service.CalculatorService;
import com.sysgears.simplecalculator.tokenprocessors.evaluator.TokenListEvaluator;
import com.sysgears.simplecalculator.tokenprocessors.sorter.TokenListPostfixConverter;
import com.sysgears.simplecalculator.tokenprocessors.parser.ExpressionParser;

import java.util.List;

/**
 * Main facade class to provide simplified interface to the calculator service
 */
public class CalculatorFacade {

    /**  Calculator service */
    private final CalculatorService service;

    /**
     * Creates an instance of the facade class
     */
    public CalculatorFacade() {
        ExpressionParser parser = new ExpressionParser();
        TokenListEvaluator evaluator = new TokenListEvaluator();
        TokenListPostfixConverter converter = new TokenListPostfixConverter();
        IHistory history = new CalculationHistoryDecorator(new CalculationHistoryContainer());
        this.service = new CalculatorService(parser, converter, evaluator, history);
    }

    /**
     * Process string expression.
     * <p>
     * In case of error will return an error message string.
     * For more information about acceptable commands and expression format see
     * @see com.sysgears.simplecalculator.service.CalculatorService
     *
     * @param expression math expression or program command, not null
     * @return list of result strings, not null
     */
    public List<String> processExpression(final String expression) {
        return service.processExpression(expression);
    }
}
