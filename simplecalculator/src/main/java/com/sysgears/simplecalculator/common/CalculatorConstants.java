package com.sysgears.simplecalculator.common;

/**
 * Command line constants holder
 */
public abstract class CalculatorConstants {

    /** Default operator priority */
    public static final int OPERATOR_PRIORITY_DEFAULT = 0;

    /**
     * Complete history receiving command
     */
    public static final String COMMAND_GET_HISTORY = "-h";

    /**
     * Unique history receiving command
     */
    public static final String COMMAND_GET_HISTORY_UNIQUE = "-hu";

    /**
     * Last history item receiving command
     */
    public static final String COMMAND_GET_HISTORY_LAST = "-hl";
}
